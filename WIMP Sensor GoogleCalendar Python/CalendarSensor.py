from datetime import date, datetime
from icalevents import icalevents


class CalendarSensor:

    def __init__(self,
                 ics_url: str = "https://calendar.google.com/calendar/ical/wimp.today%40gmail.com/public/basic.ics"):
        """

        :type ics_url: str
        """
        self.ics_url = ics_url

    def has_meeting(self, instance=datetime.now()):
        events = icalevents.events(url=self.ics_url, file=None, start=instance, end=instance)
        return len(events) > 0
