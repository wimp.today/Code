# How to install

**Requires Python 3.**

Inspired by https://stackoverflow.com/a/12657803/1168342. Note that `--python=/usr/bin/python3.6` is to force 
it to use Python 3. If your Python 3 is somewhere else, change that part of the command.

```bash
virtualenv --python=/usr/bin/python3.6 .env && source .env/bin/activate && pip install -r requirements.txt`
```

# Run tests

```bash
python CalendarSensor_test.py
```

# Possible integration with MQTT

```plantuml
@startuml DesignSequenceDiagram
skinparam style strictuml
skinparam SequenceMessageAlignment center
participant Driver as D
participant ":Calendar\nSensor" as cs
participant ":MQTT\nBroker" as broker
loop !finished
D -> cs : hasMeeting = has_meeting(now)
D -> broker : publishTopic("MyProfHasMeeting", hasMeeting)
note over D
Wait some time
(5 mins?)
end note
end loop
@enduml
```
