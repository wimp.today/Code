import unittest
from CalendarSensor import CalendarSensor
from datetime import datetime

ICS_URL: str = 'https://gitlab.com/wimp.today/Code/-/raw/master/WIMP%20Sensor%20GoogleCalendar%20Python/basic.ics'


class CalendarSensorTest(unittest.TestCase):
    def test_init(self):
        cs = CalendarSensor(ICS_URL)
        self.assertEqual(cs.ics_url, ICS_URL, 'URLs match')

    def test_has_meeting(self):
        cs = CalendarSensor(ICS_URL)
        before_beer_time = datetime(year=2020, month=3, day=4, hour=21, minute=59)
        during_beer_time = datetime(year=2020, month=3, day=4, hour=22, minute=4)
        after_beer_time = datetime(year=2020, month=3, day=5, hour=0, minute=1)
        self.assertFalse(cs.has_meeting(before_beer_time), 'no meeting before beer time')
        self.assertTrue(cs.has_meeting(during_beer_time), 'has meeting -- it is beer time')
        self.assertFalse(cs.has_meeting(after_beer_time), 'no meeting after beer time')


if __name__ == '__main__':
    unittest.main()
