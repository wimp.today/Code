package ca.concordia.wimp.mqtt.client.emitter;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Emitter {
	public static void main(final String[] args) throws MqttException {
		final Emitter emitter = new Emitter();
		emitter.connect();
		emitter.publish("sensors/temperature", String.valueOf(Math.random()));
		emitter.disconnect();
	}

	private IMqttClient client;
	public void connect() throws MqttException {
		if (this.client != null) {
			System.err.println("Connecting an already-connected client!");
		} else {
			this.client = new MqttClient("tcp://localhost:1883",
					MqttClient.generateClientId());
			this.client.setCallback(new MqttCallback() {
				@Override
				public void messageArrived(final String s, final MqttMessage m)
						throws Exception {

					System.out.println("Message received:\n\t"
							+ new String(m.getPayload()));
				}

				@Override
				public void deliveryComplete(final IMqttDeliveryToken t) {
				}

				@Override
				public void connectionLost(final Throwable t) {
					System.out.println("Connection to MQTT broker lost!");
				}
			});
			client.connect();
		}
	}
	public void publish(final String topic, final String payLoad)
			throws MqttException {
		if (this.client != null) {
			this.client.publish(topic, new MqttMessage(payLoad.getBytes()));
		} else {
			System.err.println("Publishing to an unconnected client!");
		}
	}
	public void disconnect() throws MqttException {
		if (this.client != null) {
			this.client.disconnect();
			this.client.close();
			this.client = null;
		} else {
			System.err.println("Disconnecting an unconnected client!");
		}
	}
}
