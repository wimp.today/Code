package WIMP.COAP.server.main;


import java.net.SocketException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.server.resources.CoapExchange;


public class Server extends CoapServer{

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		
		  try {
		        // create server binds on UDP port 5683
		        Server server = new Server();
		        server.start();
		    } catch (SocketException e) {
		        System.err.println("Failed to initialize server: " + e.getMessage());
		    }
	}
		  
		  public Server() throws SocketException {

		        // provide an instance of a resource
		        add(new WimpHelloResource());
		    }
	}

class WimpHelloResource extends CoapResource {

    
    /**
     * Instantiates a new publish resource.
     */
    public WimpHelloResource() {
        // set resource identifier
        super("Wimp");
        // set display name
        getAttributes().setTitle("wimp-hello Resource");
    }

    @Override
    public void handlePOST(CoapExchange exchange) {         
        System.out.println(exchange.getRequestText());          
        exchange.respond("POST_Hello From WIMP COAP Server");           
    }
    
    @Override
    public void handleGET(CoapExchange exchange) {                      
        exchange.respond("GET-Hello From WIMP COAP Server");            
    }
}



