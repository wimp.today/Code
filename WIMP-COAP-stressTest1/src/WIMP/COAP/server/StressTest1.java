package WIMP.COAP.server;


import junit.framework.TestCase;

import WIMP.COAP.server.main.Server;
import WIMP.COAP.client.main.WimpHelloClient;



public class StressTest1 extends TestCase {
	
	public void test1() {
		// Start the Server
		Server.main(null);

		// Start one Client
		WimpHelloClient.main(null);

		
		final long currentTime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			WimpHelloClient.main(null);
		}
		System.out.print("100 messages sent in ");
		System.out.print(System.currentTimeMillis() - currentTime);
		System.out.println(" ms.");
	}
	
	

}
