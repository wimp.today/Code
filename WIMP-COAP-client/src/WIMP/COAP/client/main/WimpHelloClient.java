package WIMP.COAP.client.main;

import java.io.IOException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.elements.exception.ConnectorException;

public class WimpHelloClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CoapClient client = new CoapClient("coap://localhost/Wimp");
        
        CoapResponse response = null;
		
        try {
			response = client.get();
		} catch (ConnectorException e) {
			 System.err.println("Failed to rget from client " + e.getMessage());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Failed to get from client " + e.getMessage());
		}
		
        if (response!=null) {
        
        	System.out.println( response.getCode() );
        	System.out.println( response.getOptions() );
        	System.out.println( response.getResponseText() );
        	
        } else {
        	
        	System.out.println("Request failed");
        	
        }
    }
}


