/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner;

import java.io.IOException;

import org.deeplearning4j.optimize.api.TrainingListener;
import org.deeplearning4j.rl4j.learning.sync.qlearning.QLearning;
import org.deeplearning4j.rl4j.network.dqn.DQNFactoryStdDense;
import org.deeplearning4j.rl4j.util.DataManager;
import org.nd4j.linalg.learning.config.Adam;

public final class Configuration {
	private static final QLearning.QLConfiguration Q_LEARNING = new QLearning.QLConfiguration( //
			123, // Random seed
			100000, // Max number of steps by epoch
			5000, // Max number of steps
			10000, // Max size of experience replay
			32, // size of batches
			100, // target update (hard)
			0, // num step noop warmup
			0.05, // reward scaling
			0.99, // gamma
			10.0, // td-error clipping
			0.1f, // min epsilon
			2000, // num step for eps greedy anneal
			true // double DQN
	);

	private static final DataManager DATA_MANAGER;
	static {
		DataManager temp;
		try {
			temp = new DataManager();
		} catch (final IOException e) {
			e.printStackTrace();
			temp = null;
		}
		DATA_MANAGER = temp;
	}

	private final DQNFactoryStdDense.Configuration dqnFactory;

	public Configuration() {
		this(null);
	}

	public Configuration(final TrainingListener aTrainingListener) {
		this.dqnFactory = DQNFactoryStdDense.Configuration.builder().l2(0.01).updater(new Adam(1e-2)).numLayer(3)
				.numHiddenNodes(16).listeners(new TrainingListener[] { aTrainingListener }).build();
	}

	public DQNFactoryStdDense.Configuration getDQNFactory() {
		return this.dqnFactory;
	}

	public QLearning.QLConfiguration getQLConfiguration() {
		return Configuration.Q_LEARNING;
	}

	public DataManager getDataManager() {
		return Configuration.DATA_MANAGER;
	}
}
