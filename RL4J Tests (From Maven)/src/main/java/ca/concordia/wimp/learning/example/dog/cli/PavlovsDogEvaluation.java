/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.cli;

import java.io.IOException;

import org.deeplearning4j.rl4j.policy.DQNPolicy;

import ca.concordia.wimp.learning.example.dog.learner.MarkovDecisionProcess;
import ca.concordia.wimp.learning.example.dog.learner.State;
import ca.concordia.wimp.learning.example.dog.learner.events.AbstractEvent;
import ca.concordia.wimp.learning.example.dog.learner.events.MDPListener;

public class PavlovsDogEvaluation {
	public static void main(final String[] someArgs) throws IOException {
		PavlovsDogEvaluation.evaluate("tmp/Policy");
	}

	public static void evaluate(final String aPolicyFileName) throws IOException {
		final DQNPolicy<State> policy = DQNPolicy.load(aPolicyFileName);
		final MarkovDecisionProcess mdp2 = new MarkovDecisionProcess();
		mdp2.addListener(new MDPListener() {
			public void update(final AbstractEvent anEvent) {
				System.err.println(anEvent);
			}
		});

		final int ITERATIONS = 1000;
		double rewards = 0;
		for (int i = 0; i < ITERATIONS; i++) {
			mdp2.reset();
			double reward = policy.play(mdp2);
			rewards += reward;
			System.out.println(reward);
		}
		System.out.println(rewards / ITERATIONS);

		mdp2.close();
	}
}
