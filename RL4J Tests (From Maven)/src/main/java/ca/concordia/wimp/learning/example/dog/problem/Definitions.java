/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.problem;

public interface Definitions {
	// The room and some constants to define its main elements
	int WALL = 1;
	int BONE = 8;
	int DOG = 6;
	int[][] ROOM = new int[][] { // The room with the values of the elements above
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // Row 0
			{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 }, // Row 1
			{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 }, // Row 2
			{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 }, // Row 3
			{ 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1 }, // Row 4
			{ 1, 0, 0, 0, 1, 8, 0, 0, 0, 0, 1 }, // Row 5
			{ 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1 }, // Row 6
			{ 1, 0, 6, 0, 0, 0, 0, 0, 0, 0, 1 }, // Row 7
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, // Row 8
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, // Row 9
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // Row 10
	};

	// The dog's possible action
	Coordinates[] DOG_ACTIONS = new Coordinates[] { //
			new Coordinates(-1, -1), new Coordinates(0, -1), new Coordinates(1, -1), // North-west, north, north-east
			new Coordinates(-1, 0), new Coordinates(1, 0), // west and east
			new Coordinates(-1, 1), new Coordinates(0, 1), new Coordinates(1, 1) }; // South-west, south, south-east

	// The dog's possible rewards
	int REWARD_HIT_A_WALL = -10;
	int REWARD_ATE_A_BONE = 100;
	int REWARD_EXPLORING = 1;
}
