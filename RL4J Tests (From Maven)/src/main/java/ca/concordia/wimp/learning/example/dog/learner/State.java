/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.dog.learner;

import org.deeplearning4j.rl4j.space.Encodable;

import ca.concordia.wimp.learning.example.dog.problem.Coordinates;
import ca.concordia.wimp.learning.example.dog.problem.Definitions;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

public class State implements Encodable {
	private static final double[] ConvertMatrixToArray(final double[][] matrix) {
		final double array[] = new double[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			final double[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				array[i * row.length + j] = matrix[i][j];
			}
		}

		return array;
	}

	private final double[][] tiles = new double[Definitions.ROOM[0].length][Definitions.ROOM.length];
	private Coordinates coordinates;

	public State(final int aDogCol, final int aDogRow) {
		// I don't forget to remove -1 from both column and row
		// because arrays in Java start at (0, 0), not (1, 1).

		// Remove the dog if it was somewhere before...
		if (coordinates != null) {
			this.tiles[this.coordinates.getCol() - 1][this.coordinates.getRow() - 1] = 0;
		}

		// Put the dog in its new location
		this.tiles[aDogCol - 1][aDogRow - 1] = 1;
		this.coordinates = new Coordinates(aDogCol, aDogRow);
	}

	public State(final Coordinates someCoordinates) {
		this(someCoordinates.getCol(), someCoordinates.getRow());
	}

	public int getCurrentDogCol() {
		return this.coordinates.getCol();
	}

	public int getCurrentDogRow() {
		return this.coordinates.getRow();
	}

	public double[] toArray() {
		return State.ConvertMatrixToArray(tiles);
	}

	@Override
	public String toString() {
		return this.coordinates.toString();
	}
}