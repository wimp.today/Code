/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner;

import ca.concordia.wimp.learning.example.dog.problem.Coordinates;
import ca.concordia.wimp.learning.example.dog.problem.Definitions;

public class FunctionAction {
	private Coordinates coordinates;

	public FunctionAction(final State aState, final int anActionIndex) {
		if (0 <= anActionIndex && anActionIndex < Definitions.DOG_ACTIONS.length) {
			this.coordinates = new Coordinates(
					aState.getCurrentDogCol() + Definitions.DOG_ACTIONS[anActionIndex].getCol(),
					aState.getCurrentDogRow() + Definitions.DOG_ACTIONS[anActionIndex].getRow());
		} else {
			throw new RuntimeException("This dog belongs to Schrödinger!");
		}
	}

	public int getNewDogCol() {
		return this.coordinates.getCol();
	}

	public int getNewDogRow() {
		return this.coordinates.getRow();
	}
}
