/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner.events;

public class EventDogStayed extends AbstractEvent {
	public EventDogStayed(final int aPrevCol, final int aPrevRow, final int aNewCol, final int aNewRow) {
		super(aPrevCol, aPrevRow, aNewCol, aNewRow);
	}
}
