/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;

import ca.concordia.wimp.learning.example.dog.learner.Configuration;
import ca.concordia.wimp.learning.example.dog.learner.MarkovDecisionProcess;
import ca.concordia.wimp.learning.example.dog.learner.State;
import ca.concordia.wimp.learning.example.dog.learner.events.AbstractEvent;
import ca.concordia.wimp.learning.example.dog.learner.events.MDPListener;
import ca.concordia.wimp.learning.example.dog.problem.Definitions;

public class PavlovsDogUI {
	private final class UIMDPListener implements MDPListener {
		public void update(final AbstractEvent anEvent) {
			final int numberOfColsPerRow = Definitions.ROOM[0].length;

			// Remove dog
			final int prevCol = anEvent.getPrevCol();
			final int prevRow = anEvent.getPrevRow();
			int labelPos = prevRow * numberOfColsPerRow + prevCol;
			JLabel label = (JLabel) PavlovsDogUI.this.panelRoom.getComponent(labelPos);
			if (Definitions.ROOM[prevRow][prevCol] == Definitions.BONE) {
				label.setIcon(PavlovsDogUI.ICON_BONE);
			}
			else {
				label.setIcon(null);
				label.setText(PavlovsDogUI.CreateLabel(prevCol, prevRow));
			}

			// (Re)Add bone and god
			final int newCol = anEvent.getNewCol();
			final int newRow = anEvent.getNewRow();
			labelPos = newRow * numberOfColsPerRow + newCol;
			label = (JLabel) PavlovsDogUI.this.panelRoom.getComponent(labelPos);
			label.setText(null);
			label.setIcon(PavlovsDogUI.ICON_DOG);
		}
	}

	private final class UITrainingListener implements TrainingListener {
		private static final String MODEL_PATH = "./TrainedModel.zip";
		private double previousScore = 0.0;

		public void iterationDone(final Model model, final int iteration, final int epoch) {
			PavlovsDogUI.this.fieldIterationNumberTraining.setText(String.valueOf(iteration));
			PavlovsDogUI.this.fieldEpochNumberTraining.setText(String.valueOf(epoch));

			// TODO This code should not be there but in the learner package somewhere...
			final double score = model.score();
			if (score > previousScore) {
				try {
					ModelSerializer.writeModel(model, UITrainingListener.MODEL_PATH, true);
					this.previousScore = score;
				}
				catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}

		public void onEpochStart(final Model model) {
		}

		public void onEpochEnd(final Model model) {
		}

		public void onForwardPass(final Model model, final List<INDArray> activations) {
		}

		public void onForwardPass(final Model model, final Map<String, INDArray> activations) {
		}

		public void onGradientCalculation(final Model model) {
		}

		public void onBackwardPass(final Model model) {
		}
	}

	private final class UISwingWorker extends SwingWorker<Object, Void> {
		@Override
		protected Object doInBackground() throws Exception {
			final MarkovDecisionProcess mdp = new MarkovDecisionProcess();
			final Configuration configuration = new Configuration(new UITrainingListener());
			final QLearningDiscreteDense<State> qldd = new QLearningDiscreteDense<State>(mdp,
					configuration.getDQNFactory(), configuration.getQLConfiguration(), configuration.getDataManager());
			mdp.setFetchable(qldd);
			mdp.addListener(new UIMDPListener());
			qldd.train();
			mdp.close();

			return null;
		}
	}

	private static final Color COLOR_TILE = Color.WHITE;
	private static final Color COLOR_TILE_TEXT = Color.LIGHT_GRAY;
	private static final Color COLOR_WALL = new Color(133, 94, 66);
	private static final Color COLOR_WALL_TEXT = new Color(113, 74, 46);
	private static final Icon ICON_DOG = new ImageIcon(PavlovsDogUI.class.getResource("/Dog.png"));
	private static final Icon ICON_BONE = new ImageIcon(PavlovsDogUI.class.getResource("/Bone.png"));

	private static String CreateLabel(final int col, final int row) {
		final StringBuilder builder = new StringBuilder();
		builder.append('(');
		builder.append(col);
		builder.append(", ");
		builder.append(row);
		builder.append(')');
		return builder.toString();
	}

	public static void main(final String[] someArgs) {
		final PavlovsDogUI ui = new PavlovsDogUI();
		ui.buildRoomPanel();
		ui.buildControlPanel();
		ui.buildMainFrame();
	}

	private final SwingWorker<Object, Void> worker;
	private final JFrame frameMain = new JFrame("Pavlov's Dog");
	private final JPanel panelRoom = new JPanel();
	private final JPanel panelControls = new JPanel();
	private final JTextField fieldIterationNumberTraining = new JTextField();
	private final JTextField fieldEpochNumberTraining = new JTextField();
	private final JTextField fieldIterationNumberApplying = new JTextField();
	private final JTextField fieldEpochNumberApplying = new JTextField();

	public PavlovsDogUI() {
		this.worker = new UISwingWorker();
	}

	private void buildRoomPanel() {
		final int numberOfRows = Definitions.ROOM.length;
		final int numberOfCols = Definitions.ROOM[0].length;

		this.panelRoom.setBackground(PavlovsDogUI.COLOR_WALL_TEXT);
		this.panelRoom.setBorder(
				BorderFactory.createCompoundBorder(new EmptyBorder(4, 4, 2, 4), BorderFactory.createEtchedBorder()));

		this.panelRoom.setLayout(new GridLayout(numberOfRows, numberOfCols));

		for (int row = 0; row < numberOfRows; row++) {
			for (int col = 0; col < numberOfCols; col++) {
				final JLabel label = new JLabel();
				label.setHorizontalAlignment(SwingConstants.CENTER);
				label.setFont(label.getFont().deriveFont(11.0f));
				label.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
				label.setOpaque(true);

				if (Definitions.ROOM[row][col] == Definitions.WALL) {
					label.setForeground(PavlovsDogUI.COLOR_WALL_TEXT);
					label.setBackground(PavlovsDogUI.COLOR_WALL);
					label.setText(PavlovsDogUI.CreateLabel(col, row));
				}
				else if (Definitions.ROOM[row][col] == Definitions.DOG) {
					label.setForeground(PavlovsDogUI.COLOR_TILE_TEXT);
					label.setBackground(PavlovsDogUI.COLOR_TILE);
					label.setIcon(PavlovsDogUI.ICON_DOG);
				}
				else if (Definitions.ROOM[row][col] == Definitions.BONE) {
					label.setBackground(PavlovsDogUI.COLOR_TILE);
					label.setIcon(PavlovsDogUI.ICON_BONE);
				}
				else {
					label.setForeground(PavlovsDogUI.COLOR_TILE_TEXT);
					label.setBackground(PavlovsDogUI.COLOR_TILE);
					label.setText(PavlovsDogUI.CreateLabel(col, row));
				}

				this.panelRoom.add(label);
			}
		}
	}

	private void buildControlPanel() {
		this.panelControls.setLayout(new GridLayout(1, 2));

		// Training controls
		{
			final JPanel trainingControls = new JPanel();
			trainingControls.setBackground(PavlovsDogUI.COLOR_WALL_TEXT);
			trainingControls.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(2, 4, 4, 2),
					BorderFactory.createEtchedBorder()));
			trainingControls.setLayout(new GridLayout(3, 1));

			final JLabel title1 = new JLabel("Training", JLabel.RIGHT);
			title1.setForeground(COLOR_TILE);
			trainingControls.add(title1);
			final JLabel title2 = new JLabel(" Model");
			title2.setForeground(COLOR_TILE);
			trainingControls.add(title2);

			final JButton trainingStartButton = new JButton("Start");
			trainingStartButton.setBackground(PavlovsDogUI.COLOR_TILE);
			trainingStartButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PavlovsDogUI.this.worker.execute();
				}
			});
			trainingControls.add(trainingStartButton);

			final JButton trainingStopButton = new JButton("Stop");
			trainingStopButton.setBackground(PavlovsDogUI.COLOR_TILE);
			trainingStopButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// PavlovsDogUI.this.worker.cancel(true);
				}
			});
			trainingControls.add(trainingStopButton);

			final JPanel trainingPanelIterationNumber = new JPanel(new GridLayout(1, 2));
			trainingPanelIterationNumber.setBackground(PavlovsDogUI.COLOR_TILE);
			trainingPanelIterationNumber.add(new JLabel("Iteration #"));
			this.fieldIterationNumberTraining.setEditable(false);
			this.fieldIterationNumberTraining.setOpaque(false);
			trainingPanelIterationNumber.add(this.fieldIterationNumberTraining);
			trainingControls.add(trainingPanelIterationNumber);

			final JPanel trainingPanelEpochNumber = new JPanel(new GridLayout(1, 2));
			trainingPanelEpochNumber.setBackground(PavlovsDogUI.COLOR_TILE);
			trainingPanelEpochNumber.add(new JLabel("Epoch #"));
			this.fieldEpochNumberTraining.setEditable(false);
			this.fieldEpochNumberTraining.setOpaque(false);
			trainingPanelEpochNumber.add(fieldEpochNumberTraining);
			trainingControls.add(trainingPanelEpochNumber);

			this.panelControls.add(trainingControls);
		}

		// Applying controls
		{
			final JPanel applyingControls = new JPanel();
			applyingControls.setBackground(PavlovsDogUI.COLOR_WALL_TEXT);
			applyingControls.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(2, 2, 4, 4),
					BorderFactory.createEtchedBorder()));
			applyingControls.setLayout(new GridLayout(3, 1));

			final JLabel title1 = new JLabel("Applying", JLabel.RIGHT);
			title1.setForeground(COLOR_TILE);
			applyingControls.add(title1);
			final JLabel title2 = new JLabel(" Model");
			title2.setForeground(COLOR_TILE);
			applyingControls.add(title2);

			final JButton applyingStartButton = new JButton("Start");
			applyingStartButton.setBackground(PavlovsDogUI.COLOR_TILE);
			applyingStartButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// PavlovsDogUI.this.worker.execute();
				}
			});
			applyingControls.add(applyingStartButton);

			final JButton applyingStopButton = new JButton("Stop");
			applyingStopButton.setBackground(PavlovsDogUI.COLOR_TILE);
			applyingStopButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// PavlovsDogUI.this.worker.cancel(true);
				}
			});
			applyingControls.add(applyingStopButton);

			final JPanel applyingPanelIterationNumber = new JPanel(new GridLayout(1, 2));
			applyingPanelIterationNumber.setBackground(PavlovsDogUI.COLOR_TILE);
			applyingPanelIterationNumber.add(new JLabel("Iteration #"));
			this.fieldIterationNumberApplying.setEditable(false);
			this.fieldIterationNumberApplying.setOpaque(false);
			applyingPanelIterationNumber.add(this.fieldIterationNumberApplying);
			applyingControls.add(applyingPanelIterationNumber);

			final JPanel applyingPanelEpochNumber = new JPanel(new GridLayout(1, 2));
			applyingPanelEpochNumber.setBackground(PavlovsDogUI.COLOR_TILE);
			applyingPanelEpochNumber.add(new JLabel("Epoch #"));
			this.fieldEpochNumberApplying.setEditable(false);
			this.fieldEpochNumberApplying.setOpaque(false);
			applyingPanelEpochNumber.add(fieldEpochNumberApplying);
			applyingControls.add(applyingPanelEpochNumber);

			this.panelControls.add(applyingControls);
		}
	}

	private void buildMainFrame() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (final Exception e) {
			// Nothing to...
		}

		this.frameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.frameMain.add(this.panelRoom, BorderLayout.NORTH);
		this.frameMain.add(this.panelControls, BorderLayout.SOUTH);
		this.frameMain.pack();

		final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.frameMain.setLocation(dim.width / 2 - this.frameMain.getSize().width / 2,
				dim.height / 2 - this.frameMain.getSize().height / 2);

		this.frameMain.setVisible(true);
	}
}
