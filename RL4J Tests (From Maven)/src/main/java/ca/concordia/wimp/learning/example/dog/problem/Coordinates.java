/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.problem;

public class Coordinates {
	private final int col;
	private final int row;

	public Coordinates(final int aCol, final int aRow) {
		this.col = aCol;
		this.row = aRow;
	}

	public int getCol() {
		return this.col;
	}

	public int getRow() {
		return this.row;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append('(');
		builder.append(this.col);
		builder.append(", ");
		builder.append(this.row);
		builder.append(')');
		return builder.toString();
	}
}
