/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.toy3;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.learning.NeuralNetFetchable;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

public class MarkovDecisionProcess implements MDP<State, Integer, DiscreteSpace> {

	// Arbitrary number of steps allowed per iterations, the greater the slower but
	// the more reward.
	private static final int MAX_STEP = 20;

	private State state;
	private boolean isDone;

	@Getter
	private DiscreteSpace actionSpace = new DiscreteSpace(2);

	@Getter
	private ObservationSpace<State> observationSpace = new ArrayObservationSpace<State>(new int[] { 1 });

	@SuppressWarnings("rawtypes")
	@Setter
	private NeuralNetFetchable<IDQN> fetchable;

	public void close() {
	}

	public boolean isDone() {
		return this.isDone;
	}

	public MarkovDecisionProcess newInstance() {
		final MarkovDecisionProcess mdp = new MarkovDecisionProcess();
		mdp.setFetchable(fetchable);
		return mdp;
	}

	public State reset() {
		this.isDone = false;
		return state = new State(0, 0);
	}

	public StepReply<State> step(final Integer a) {
		final double reward = (state.getStep() % 2 == 0) ? 1 - a : a;
		this.state = new State(this.state.getI() + 1, this.state.getStep() + 1);
		this.isDone = this.state.getStep() == MAX_STEP;
		return new StepReply<State>(this.state, reward, this.isDone, new JSONObject("{}"));
	}
}
