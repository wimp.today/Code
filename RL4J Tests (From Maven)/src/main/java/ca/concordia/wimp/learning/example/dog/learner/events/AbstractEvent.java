/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner.events;

public class AbstractEvent {
	private final int prevCol;
	private final int prevRow;
	private final int newCol;
	private final int newRow;

	public AbstractEvent(final int aPrevCol, final int aPrevRow, final int aNewCol, final int aNewRow) {
		this.prevCol = aPrevCol;
		this.prevRow = aPrevRow;
		this.newCol = aNewCol;
		this.newRow = aNewRow;
	}

	public int getPrevCol() {
		return this.prevCol;
	}

	public int getPrevRow() {
		return this.prevRow;
	}

	public int getNewCol() {
		return this.newCol;
	}

	public int getNewRow() {
		return this.newRow;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append('(');
		builder.append(this.prevCol);
		builder.append(", ");
		builder.append(this.prevRow);
		builder.append(") -> ");
		builder.append('(');
		builder.append(this.newCol);
		builder.append(", ");
		builder.append(this.newRow);
		builder.append(") with: ");
		builder.append(this.getClass().getSimpleName());
		return builder.toString();
	}
}
