/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner;

import ca.concordia.wimp.learning.example.dog.problem.Definitions;

public class FunctionReward {
	private final State state;
	private final int reward;
	private final boolean done;

	public FunctionReward(final State aCurrentState, final int anExpectedNewDogCol, final int anExpectedNewDogRow) {
		if (Definitions.ROOM[anExpectedNewDogRow][anExpectedNewDogCol] == Definitions.WALL) {
			// If the dog bangs into a wall, then it cannot move...
			this.state = new State(aCurrentState.getCurrentDogCol(), aCurrentState.getCurrentDogRow());
			// ... and it is penalized!
			this.reward = Definitions.REWARD_HIT_A_WALL;
			this.done = false;
		} else if (Definitions.ROOM[anExpectedNewDogRow][anExpectedNewDogCol] == Definitions.BONE) {
			// If the dog found the chicken drumstick,
			// the it is rewarded and the learning is done.
			this.state = new State(anExpectedNewDogCol, anExpectedNewDogRow);
			this.reward = Definitions.REWARD_ATE_A_BONE;
			this.done = true;
		} else {
			this.state = new State(anExpectedNewDogCol, anExpectedNewDogRow);
			this.reward = Definitions.REWARD_EXPLORING;
			this.done = false;
		}
	}

	public State getNewState() {
		return this.state;
	}

	public int getReward() {
		return this.reward;
	}

	public boolean isDone() {
		return this.done;
	}
}
