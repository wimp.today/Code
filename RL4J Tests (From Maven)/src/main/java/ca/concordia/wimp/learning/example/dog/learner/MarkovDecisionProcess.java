/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.dog.learner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.learning.NeuralNetFetchable;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.json.JSONObject;

import ca.concordia.wimp.learning.example.dog.learner.events.AbstractEvent;
import ca.concordia.wimp.learning.example.dog.learner.events.EventActionChosen;
import ca.concordia.wimp.learning.example.dog.learner.events.EventBoneEaten;
import ca.concordia.wimp.learning.example.dog.learner.events.EventDogMoved;
import ca.concordia.wimp.learning.example.dog.learner.events.EventDogStayed;
import ca.concordia.wimp.learning.example.dog.learner.events.EventStateReset;
import ca.concordia.wimp.learning.example.dog.learner.events.MDPListener;
import ca.concordia.wimp.learning.example.dog.problem.Definitions;
import lombok.Getter;
import lombok.Setter;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

public class MarkovDecisionProcess implements MDP<State, Integer, DiscreteSpace> {
	private List<MDPListener> listeners = new ArrayList<MDPListener>();
	private State state;
	private double reward;
	private boolean done;

	@Getter
	private DiscreteSpace actionSpace = new DiscreteSpace(Definitions.DOG_ACTIONS.length);

	@Getter
	private ObservationSpace<State> observationSpace = new ArrayObservationSpace<State>(
			new int[] { Definitions.ROOM[0].length * Definitions.ROOM.length });

	@SuppressWarnings("rawtypes")
	@Setter
	private NeuralNetFetchable<IDQN> fetchable;

	public void close() {
	}

	public boolean isDone() {
		return this.done;
	}

	public MarkovDecisionProcess newInstance() {
		final MarkovDecisionProcess mdp = new MarkovDecisionProcess();
		mdp.setFetchable(fetchable);
		return mdp;
	}

	public State reset() {
		this.state = new StateOrigin();
		this.reward = 0;
		this.done = false;

		// Notify the listeners
		final AbstractEvent event = new EventStateReset(((StateOrigin) this.state).getBoneCol(),
				((StateOrigin) this.state).getBoneRow(), this.state.getCurrentDogCol(), this.state.getCurrentDogRow());
		this.notifyListeners(event);

		return this.state;
	}

	public StepReply<State> step(final Integer anActionIndex) {
		final int previousDogCol = this.state.getCurrentDogCol();
		final int previousDogRow = this.state.getCurrentDogRow();

		// Get the new coordinates, whatever they are
		final FunctionAction actionFunction = new FunctionAction(this.state, anActionIndex);

		// Notification of the listeners of a chosen action
		AbstractEvent event = new EventActionChosen(previousDogCol, previousDogRow, previousDogCol, previousDogRow,
				anActionIndex);
		this.notifyListeners(event);

		// Check new coordinates and decide of a reward and whether the dog ate the bone
		final FunctionReward rewardFunction = new FunctionReward(this.state, actionFunction.getNewDogCol(),
				actionFunction.getNewDogRow());
		this.state = rewardFunction.getNewState();
		this.reward = rewardFunction.getReward();
		this.done = rewardFunction.isDone();

		final int currentDogCol = this.state.getCurrentDogCol();
		final int currentDogRow = this.state.getCurrentDogRow();

		// Notification of the listeners of a movement (or lack thereof)
		if (previousDogCol == currentDogCol && previousDogRow == currentDogRow) {
			event = new EventDogStayed(previousDogCol, previousDogRow, currentDogCol, currentDogRow);
		}
		else {
			event = new EventDogMoved(previousDogCol, previousDogRow, currentDogCol, currentDogRow);
		}
		this.notifyListeners(event);

		// Notification of the listeners if the dog ate the bone
		if (this.done) {
			event = new EventBoneEaten(previousDogCol, previousDogRow, currentDogCol, currentDogRow);
			this.notifyListeners(event);
		}

		return new StepReply<State>(this.state, this.reward, this.done, new JSONObject("{}"));
	}

	public void addListener(final MDPListener aListener) {
		this.listeners.add(aListener);
	}

	private void notifyListeners(final AbstractEvent anEvent) {
		for (Iterator<MDPListener> iterator = this.listeners.iterator(); iterator.hasNext();) {
			final MDPListener listener = iterator.next();
			listener.update(anEvent);
		}
	}
}
