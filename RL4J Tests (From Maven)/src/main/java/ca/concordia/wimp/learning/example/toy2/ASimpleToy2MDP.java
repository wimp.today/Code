/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.toy2;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.learning.NeuralNetFetchable;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.json.JSONObject;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 *
 *         A toy MDP where reward are given in every case. Useful to debug
 */
@Slf4j
public class ASimpleToy2MDP implements MDP<ASimpleToy2State, Integer, DiscreteSpace> {

	final private int maxStep;
	// TODO 10 steps toy (always +1 reward2 actions), toylong (1000 steps),
	// toyhard (7 actions, +1 only if action = (step/100+step)%7, and toyStoch
	// (like last but reward has 0.10 odd to be somewhere else).
	@Getter
	private DiscreteSpace actionSpace = new DiscreteSpace(2);
	@Getter
	private ObservationSpace<ASimpleToy2State> observationSpace = new ArrayObservationSpace<ASimpleToy2State>(
			new int[] { 1 });
	private ASimpleToy2State simpleToyState;
	@Setter
	private NeuralNetFetchable<IDQN> fetchable;

	public ASimpleToy2MDP(int maxStep) {
		this.maxStep = maxStep;
	}

	public void printTest(int maxStep) {
		INDArray input = Nd4j.create(maxStep, 1);
		for (int i = 0; i < maxStep; i++) {
			input.putRow(i, Nd4j.create(new ASimpleToy2State(i, i).toArray()));
		}
		INDArray output = fetchable.getNeuralNet().output(input);
		log.info(output.toString());
	}

	public void close() {
	}

	public boolean isDone() {
		return simpleToyState.getStep() == maxStep;
	}

	public ASimpleToy2State reset() {
		// if (fetchable != null)
		// printTest(maxStep);

		return simpleToyState = new ASimpleToy2State(0, 0);
	}

	public StepReply<ASimpleToy2State> step(Integer a) {
		double reward = (simpleToyState.getStep() % 2 == 0) ? 1 - a : a;
		simpleToyState = new ASimpleToy2State(simpleToyState.getI() + 1, simpleToyState.getStep() + 1);
		return new StepReply<ASimpleToy2State>(simpleToyState, reward, isDone(), new JSONObject("{}"));
	}

	public ASimpleToy2MDP newInstance() {
		ASimpleToy2MDP mdp = new ASimpleToy2MDP(maxStep);
		mdp.setFetchable(fetchable);
		return mdp;
	}

}
