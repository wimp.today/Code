/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.toy3;

import org.deeplearning4j.rl4j.space.Encodable;

import lombok.Value;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

@Value
public class State implements Encodable {
	private int i;
	private int step;

	public double[] toArray() {
		double[] ar = new double[1];
		ar[0] = (20 - i);
		return ar;
	}
}
