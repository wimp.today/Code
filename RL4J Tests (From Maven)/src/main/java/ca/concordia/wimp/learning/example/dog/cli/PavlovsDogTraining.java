/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.cli;

import java.io.IOException;

import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;
import org.deeplearning4j.rl4j.policy.DQNPolicy;

import ca.concordia.wimp.learning.example.dog.learner.Configuration;
import ca.concordia.wimp.learning.example.dog.learner.MarkovDecisionProcess;
import ca.concordia.wimp.learning.example.dog.learner.State;

public class PavlovsDogTraining {
	public static void main(final String[] someArgs) throws IOException {
		PavlovsDogTraining.train("tmp/Policy");
	}

	public static void train(final String aPolicyFileName) throws IOException {
		final MarkovDecisionProcess mdp = new MarkovDecisionProcess();
		final Configuration configuration = new Configuration();
		final QLearningDiscreteDense<State> qldd = new QLearningDiscreteDense<State>(mdp, configuration.getDQNFactory(),
				configuration.getQLConfiguration(), configuration.getDataManager());
		mdp.setFetchable(qldd);
		qldd.train();

		final DQNPolicy<State> policy = qldd.getPolicy();
		policy.save(aPolicyFileName);

		mdp.close();
	}
}
