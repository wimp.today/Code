/*******************************************************************************
 * Copyright (c) 2019 Yann-Gaël Guéhéneuc and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Yann-Gaël Guéhéneuc and others, see in file; API and its implementation
 ******************************************************************************/
package ca.concordia.wimp.learning.example.dog.learner;

import ca.concordia.wimp.learning.example.dog.problem.Coordinates;
import ca.concordia.wimp.learning.example.dog.problem.Definitions;

public class StateOrigin extends State {
	private static final Coordinates ORIGIN_DOG;
	static {
		int col = 0;
		int row = 0;
		search: for (row = 0; row < Definitions.ROOM.length; row++) {
			final int[] columns = Definitions.ROOM[row];
			for (col = 0; col < columns.length; col++) {
				if (Definitions.ROOM[row][col] == Definitions.DOG) {
					break search;
				}
			}
		}
		ORIGIN_DOG = new Coordinates(col, row);
	}
	private static final Coordinates ORIGIN_BONE;
	static {
		int col = 0;
		int row = 0;
		search: for (row = 0; row < Definitions.ROOM.length; row++) {
			final int[] columns = Definitions.ROOM[row];
			for (col = 0; col < columns.length; col++) {
				if (Definitions.ROOM[row][col] == Definitions.BONE) {
					break search;
				}
			}
		}
		ORIGIN_BONE = new Coordinates(col, row);
	}

	public StateOrigin() {
		super(StateOrigin.ORIGIN_DOG);
	}

	public int getBoneCol() {
		return StateOrigin.ORIGIN_BONE.getCol();
	}

	public int getBoneRow() {
		return StateOrigin.ORIGIN_BONE.getRow();
	}
}
