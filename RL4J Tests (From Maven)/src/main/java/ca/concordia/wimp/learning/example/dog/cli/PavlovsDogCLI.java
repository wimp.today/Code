package ca.concordia.wimp.learning.example.dog.cli;

import java.io.IOException;

public class PavlovsDogCLI {
	private static final String POLICY_FILE_NAME = "tmp/Policy";

	public static void main(final String[] args) throws IOException {
		// Train the dog
		System.out.println("********** Dog's Training ************");
		PavlovsDogTraining.train(PavlovsDogCLI.POLICY_FILE_NAME);

		// Evaluate the dog
		System.out.println("********** Dog's Evaluation **********");
		PavlovsDogEvaluation.evaluate(PavlovsDogCLI.POLICY_FILE_NAME);
	}
}
